# mdatom

## Build

```sh
mkdir build
cmake -B build
make -C build
```

## Run

```sh
build/mdatom <params.inp> (<coords.inp>)
```

For instance test1:

```sh
build/mdatom res/test1_params.inp res/test1_coords.inp
```
