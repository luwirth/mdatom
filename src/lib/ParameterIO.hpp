#ifndef PARAMETERIO_H
#define PARAMETERIO_H

#include "MDParameters.hpp"
#include <iostream>
#include <string>

/*
 * Class to read the parameters from a file
 */
class ParameterIO {
public:
  static MDParameters readParameters(const std::string &fileName);

  static void saveParameters(const std::string &fileName,
                             const MDParameters &par);

  static void outputParameters(std::ostream &out, const MDParameters &par);
};

#endif // PARAMETERIO_H